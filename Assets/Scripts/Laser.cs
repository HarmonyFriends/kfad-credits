﻿//#define LASER_DEBUG

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VolumetricLines;

public class Laser : MonoBehaviour {
	public float timeLength = 0.02f;
	public float bufferTime = 0.001f;

	VolumetricLineBehavior behavior;
	float initialWidth, shrinkWidth;

	public Vector3 start, end;

	float timer = 0.0f;

	[HideInInspector]
	public Credit credit;

	[HideInInspector]
	public bool shrink = false;

	[HideInInspector]
	public Vector3 hitMousePos;

	// Use this for initialization
	void Start () {
		behavior = GetComponent<VolumetricLineBehavior>();

		if (shrink)
		{
			initialWidth = behavior.LineWidth;
			shrinkWidth = initialWidth * 0.3f;
			initialWidth += shrinkWidth;
		}

		timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
#if LASER_DEBUG
#else
		if (timer >= timeLength)
		{
			Destroy(gameObject);
		}
#endif

		if (shrink)
		{
			behavior.LineWidth = Mathf.Lerp(initialWidth, shrinkWidth, timer / bufferTime);
		}

		timer += Time.smoothDeltaTime;

		float startTime = Mathf.Clamp01(timer / (timeLength - bufferTime));
#if LASER_DEBUG
#else
		behavior.StartPos = Vector3.Lerp(start, end, startTime);
#endif

		if (credit != null)
		{
			if (startTime >= 1.0f)
			{
				credit.Hit(hitMousePos);
				credit = null;
			}
		}

#if LASER_DEBUG
		behavior.StartPos = start;
		behavior.EndPos = end;
#else
		float endTime = Mathf.Clamp01((timer - bufferTime) / timeLength);
		behavior.EndPos = Vector3.Lerp(start, end, endTime);
#endif

	}
}
