﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugTime : MonoBehaviour
{
	CreditsManager manager;
	TextMeshPro textMesh;

	void Start()
	{
		manager = CreditsManager.Locate();
		textMesh = GetComponent<TextMeshPro>();
	}

	void Update()
	{
		textMesh.text = manager.debugTimer.ToString("F5");
	}
}
