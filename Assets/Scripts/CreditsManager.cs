﻿//#define RECORDING_VERSION
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using TMPro;

public class CreditsManager : MonoBehaviour {
	CreditsSet[] creditsSets;
	int creditsSetsCount;

	CreditsSet nextCreditsSet;
	int nextCreditsIndex;

	AudioSource musicSource, drumRollSource;
	AudioSource[] sfxSources;
	const int sfxSourcesCount = 16;
	int sfxSourceIndex = 0;
    bool playedMusicYet = false;

    [HideInInspector]
	public float timer = 0.0f;
	[HideInInspector]
	public float debugTimer = 0.0f;

	public float skipToTime = 0.0f;

	float nextTime = Mathf.Infinity;

	TextMeshPro percentage, percentagePopup;
	float percentageTimer, scoreTally;

	[HideInInspector]
	public static int score, oldScore, totalPossibleScore, oldTotalPossibleScore;

	public TMP_ColorGradient darkenedGradient;

	static CreditsManager instance;
	public static CreditsManager Locate()
	{
		return instance;
	}

	new public Camera camera;
	Camera uiCamera;

	public bool mouseDown;
	public Vector3 mousePositionCorrected;

	public AudioClip defaultSmallBreakSound, defaultBigChipSound, defaultBigBreakSound, defaultSwooshSound, laserSound, dullHitSound, perfectScoreClip, unlockClip;

	public static GameObject collisionBox;
	
	Material fadeMaterial, fadeMaterial2;
	float fromAlpha, toAlpha, colorTimer, colorFadeLength;

    float preStartDelay;

	public GameObject sparks, bigSparks, laser, musician;

    [HideInInspector]
	public VideoPlayer videoPlayer, videoPlayerEnd;
	bool videoStarting, videoPlaying;

	Vector3 laserStartOffset, laserStartOffset2;

	const float endXFactor = 0.5f;


	public List<Material> characterMaterials;
	Material currentCharacterMaterial, fadeInCharacterMaterial, fadeOutCharacterMaterial;
	float fadeInSmoothVelocity, fadeOutSmoothVelocity;
	int characterIndex, previousCharacterIndex;
	const float characterOpacity = 0.2f;
	const float characterFadeTime = 0.1f;

    TextMeshPro timeText;

	void Awake()
    {
        instance = this;

		camera = GameObject.FindGameObjectWithTag("CreditsCamera").GetComponent<Camera>();
		mouseDown = false;

		score = 0;
		oldScore = -1;
		totalPossibleScore = 0;
		oldTotalPossibleScore = -1;

		collisionBox = Resources.Load<GameObject>("Prefabs/Credit Collision Box");

		percentage = null;
		percentagePopup = null;
		percentageTimer = 0.0f;
		scoreTally = 0;

        Transform fadePlane2 = GameObject.FindGameObjectWithTag("FadePlane2").transform;
        fadeMaterial2 = fadePlane2.GetComponent<Renderer>().material;
        drumRollSource = fadePlane2.gameObject.GetComponent<AudioSource>();

        Transform fadePlane = GameObject.FindGameObjectWithTag("FadePlane").transform;
		fadePlane.localPosition = Vector3.zero;
#if RECORDING_VERSION
        Renderer r = fadePlane.GetComponent<Renderer>();
        r.material = fadeMaterial2;
        fadeMaterial = r.material;
#else
        fadeMaterial = fadePlane.GetComponent<Renderer>().material;
#endif
        fromAlpha = 1.0f;
		toAlpha = 0.0f;
		colorTimer = 0.0f;
		colorFadeLength = 1.0f;

        fadePlane.localPosition = Vector3.zero;
#if RECORDING_VERSION
        preStartDelay = 5.0f;
#else
        preStartDelay = 1.0f;
#endif

        GameObject videoPlayerGameObject = GameObject.FindGameObjectWithTag("BGVideo");
		if (videoPlayerGameObject != null)
		{
			videoPlayer = videoPlayerGameObject.GetComponent<VideoPlayer>();
			videoPlayer.started += OnVideoPlayerStart;
			videoStarting = false;
			videoPlaying = false;
		}

        videoPlayerEnd = GameObject.FindGameObjectWithTag("UnlockVideo").GetComponent<VideoPlayer>();
        videoPlayerEnd.Prepare();

        laserStartOffset = new Vector3(-55.0f, -150.0f, -786.0f);

		laserStartOffset2 = laserStartOffset;
		laserStartOffset2.x *= -1.0f;
		
		uiCamera = GameObject.FindGameObjectWithTag("UICamera").GetComponent<Camera>();

		Transform characterContainer = GameObject.FindGameObjectWithTag("CharacterContainer").transform;
		characterMaterials = new List<Material>();
		for (int i = 0; i < 17; i++)
		{
			Transform characterTransform = characterContainer.GetChild(i);
			characterTransform.gameObject.SetActive(true);
			Material material = characterTransform.GetComponent<Renderer>().material;
			characterMaterials.Add(material);
			material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		}
		currentCharacterMaterial = null;
		fadeInCharacterMaterial = null;
		fadeInSmoothVelocity = 0;
		fadeOutCharacterMaterial = null;
		fadeOutSmoothVelocity = 0;
		characterIndex = -1;
		previousCharacterIndex = -1;

		GameObject overscanGuide = GameObject.FindGameObjectWithTag("OverscanGuide");
		if (overscanGuide != null) Destroy(overscanGuide);

        playedMusicYet = false;
    }

	private void OnVideoPlayerStart(VideoPlayer source)
	{
		if (!videoPlaying)
		{
			videoPlaying = true;
            //musicSource.Play();
            if (videoPlayer != null)
            {
                videoPlayer.started += OnVideoPlayerStart;
                videoPlayer.seekCompleted -= OnVideoPlayerStartToSeek;
                videoPlayer.seekCompleted -= OnVideoPlayerStart;
            }
		}
	}

	private void OnVideoPlayerStartToSeek(VideoPlayer source)
	{
        if (!playedMusicYet) videoPlayer.time = skipToTime;
	}

	void Start()
	{
        timeText = GameObject.FindGameObjectWithTag("TimeText").GetComponent<TextMeshPro>();
        timeText.gameObject.SetActive(false);

		musicSource = GetComponent<AudioSource>();

		sfxSources = new AudioSource[sfxSourcesCount];
		for (int i = 0; i < sfxSourcesCount; i++)
		{
			sfxSources[i] = gameObject.AddComponent<AudioSource>();
		}

		List<string> musicians = new List<string>();
		musicians.Add("dante");
		musicians.Add("Cryptrik");
		musicians.Add("MtH");
		musicians.Add("ThePopStarDude");
		musicians.Add("Dirty Spaceman");
		musicians.Add("Harley201");
		musicians.Add("Jp");
		musicians.Add("Spingus");
		musicians.Add("LarryInc64");
		musicians.Add("trivial171");
		musicians.Add("Craz Xexe");
		musicians.Add("Peacock Roy");
		musicians.Add("Vari");
		musicians.Add("Niko+");
		musicians.Add("Half Pixel");
		musicians.Add("Zoom Guy");
		musicians.Add("QTQTQ");
		musicians.Add("wolfman1405");
		musicians.Add("Nib Roc");
		musicians.Add("UUN4");
		musicians.Add("pedipanol");
		musicians.Add("SmokyThrill77");
		musicians.Add("Mitchell");
		musicians.Add("KnightOfGames");
		musicians.Add("SoundSync5000");
		musicians.Add("Sarvéproductions");
		musicians.Add("WaluigiTime64");
		musicians.Add("Unknown Meta");
		musicians.Add("Lenox");
		musicians.Add("Chaze the Chat");
		musicians.Add("reason");
		musicians.Add("Hinchy");
		musicians.Add("Kirbio");
		musicians.Add("Lu9");
		musicians.Add("BobTheTacocat");
		musicians.Add("DeVector");
		musicians.Add("Retro Gaming");
		musicians.Add("bomberzx (PACHI87)");
		musicians.Add("SUPAHSTAR CLOD");
		musicians.Add("JerryStuff (Ro)");
		musicians.Add("Moder112");
		musicians.Add("Scribble1k");
		musicians.Add("Dead Line");
		musicians.Add("Metal Head");
		musicians.Add("SubName");
		musicians.Add("reach");
		musicians.Add("thefluffyslipper");
		musicians.Add("RHMan");
		musicians.Add("ChickenSuitGuy");
		musicians.Add("Ethan64Music");
		musicians.Add("cookiefonster");
        musicians.Add("gameonion");
        int musicianCount = musicians.Count;

		creditsSetsCount = transform.childCount;
		creditsSets = new CreditsSet[creditsSetsCount + musicianCount];
		float lastAppearanceTime = 0.0f;
		int musicianOffset = 0;
		for (int i = 0; i < creditsSetsCount; i++)
		{
			Transform child = transform.GetChild(i);
			CreditsSet set = child.GetComponent<CreditsSet>();
			if (set != null)
			{
				set.enabled = true;

				lastAppearanceTime += set.timeAfterLastAppearance;
				set.appearanceTime = lastAppearanceTime;

				creditsSets[i+musicianOffset] = set;

				child.gameObject.SetActive(true);

				child.localEulerAngles = new Vector3(child.localEulerAngles.x + 90.0f, child.localEulerAngles.y, child.localEulerAngles.z);
			}

			if (set.gameObject.name == "Musicians")
			{
				Vector3 origPosition = Vector3.zero;
				Vector3 position = Vector3.zero;

				for (int mi = 0; mi < musicianCount; mi++)
				{
					string musicianName = musicians[mi];

					GameObject musicianObject = Instantiate<GameObject>(musician);
					musicianObject.name = musicianName;

					Transform musicianTransform = musicianObject.transform;
					musicianTransform.parent = transform;
					musicianTransform.localEulerAngles = Vector3.zero;

					Transform musicianTextTransform = musicianTransform.GetChild(0);
					musicianTextTransform.GetComponent<TextMeshPro>().text = musicianName;

					if (mi == 0)
					{
						origPosition = musicianTextTransform.localPosition;
						position = origPosition;
					}
					else
					{
						if (position.z < -410)
						{
							position.z = origPosition.z;
						}
						else
						{
							position.z -= 35.0f;
                        }
                        position.x *= -1.0f;
                        musicianTextTransform.localPosition = position;
					}

					CreditsSet musicianSet = musicianObject.GetComponent<CreditsSet>();

					musicianSet.enabled = true;

					lastAppearanceTime += musicianSet.timeAfterLastAppearance;
					musicianSet.appearanceTime = lastAppearanceTime;

					creditsSets[i+mi+1] = musicianSet;
				}
				musicianOffset = musicianCount;
			}
		}
		creditsSetsCount += musicianCount;

		/*
		for (int i = 0; i < creditsSets.Length; i++)
		{
			Debug.Log(i + " => " + creditsSets[i].gameObject.name);
		}
		*/

		int firstOneIndex = 0;

		if (skipToTime > 0.0f)
		{
			for (int i = 0; i < creditsSets.Length; i++)
			{
				float startTime = creditsSets[i].appearanceTime;
				if (startTime >= skipToTime)
				{
					firstOneIndex = i;
					timer = Mathf.Min(startTime, skipToTime);
					break;
				}
			}

			if (timer <= 0.0f)
			{
				firstOneIndex = creditsSets.Length;
				timer = skipToTime;
                PlayEndVideo();

            }

            skipToTime = timer;

            preStartDelay = 0.0f;
        }

		nextCreditsIndex = firstOneIndex;
		if (nextCreditsIndex < creditsSets.Length)
		{
			nextCreditsSet = creditsSets[firstOneIndex];
			nextTime = nextCreditsSet.appearanceTime;

			if (nextCreditsIndex > 0)
			{
				for (int i = 0; i < nextCreditsIndex; i++)
				{
					Destroy(creditsSets[i].gameObject);
				}
			}
		}

		if (videoPlayer == null)
		{
			OnVideoPlayerStart(null);
		}
		else
		{
			videoPlayer.Prepare();
		}
	}

	public void Update()
	{
		if (!videoPlaying)
		{
			if (!videoStarting)
			{
				if (videoPlayer.isPrepared)
				{
                    if (preStartDelay <= 0.0f)
                    {
                        if (skipToTime > 0.0f)
                        {
                            videoPlayer.started += OnVideoPlayerStartToSeek;
                            videoPlayer.seekCompleted += OnVideoPlayerStart;

                            Cursor.visible = false;
                            CrosshairDraw.started = true;
                        }
                        else
                        {
                            videoPlayer.started += OnVideoPlayerStart;
                        }

                        videoPlayer.Play();
                        videoStarting = true;
                    }
                    else
                    {
                        preStartDelay -= Time.deltaTime;
                    }
				}
			}

			return;
		}
        else if (!playedMusicYet)
        {
            if (videoPlayer != null)
            {
                if ((float)videoPlayer.time >= skipToTime)
                {
                    playedMusicYet = true;
                    musicSource.Play();
                    musicSource.time = (float)videoPlayer.time;
                }
            }
        }

		if (colorTimer < colorFadeLength)
		{
			colorTimer += Time.deltaTime;
            
			if (colorTimer >= colorFadeLength)
			{
				// done!
				fadeMaterial.SetFloat("_Alpha", toAlpha);

				// set up ending fade to black
				fadeMaterial.mainTexture = Resources.Load<Texture>("Textures/black");
			}
			else
            {
				fadeMaterial.SetFloat("_Alpha", Mathf.Lerp(fromAlpha, toAlpha, colorTimer / colorFadeLength));
			}
		}

        if (videoPlayer != null) if(videoPlayer.isPlaying) timer = (float)videoPlayer.time;
        timer += Time.smoothDeltaTime;
		
		if (timer >= nextTime)
		{
			nextCreditsSet.transform.localEulerAngles = new Vector3(nextCreditsSet.transform.localEulerAngles.x - 90.0f, nextCreditsSet.transform.localEulerAngles.y, nextCreditsSet.transform.localEulerAngles.z);

			nextCreditsSet.Go();

			if (nextCreditsSet.gameObject.name == "SCORE")
			{
				percentageTimer = 4.4f;
			}
			else if (nextCreditsSet.gameObject.name == "End 1")
			{
				CrosshairDraw.finished = true;
			}

			characterIndex = nextCreditsSet.character;

			debugTimer = (timer - nextTime);

			nextCreditsIndex++;

			if (nextCreditsIndex < creditsSetsCount)
			{
				nextCreditsSet = creditsSets[nextCreditsIndex];
				nextTime = nextCreditsSet.appearanceTime;
			}
			else
			{
				nextTime = Mathf.Infinity;
                if (nextCreditsSet != null) nextCreditsSet.theLastOne = true;
            }
		}

		debugTimer = timer; //+= Time.smoothDeltaTime;

		if (nextCreditsIndex >= 0)
		{
			if (percentage == null)
			{
				if (percentageTimer > 0)
				{
					percentage = GameObject.FindGameObjectWithTag("Percentage").GetComponent<TextMeshPro>();
					percentagePopup = percentage.GetComponent<LargeCredit>().popup.GetComponent<TextMeshPro>();
				}
			}
			else if ((oldScore != score) || (oldTotalPossibleScore != totalPossibleScore))
			{
				oldScore = score;
				oldTotalPossibleScore = totalPossibleScore;

				percentage.text = "0.0%";
				if (score <= 0)
				{
					percentagePopup.text = "You didn't hit anything at all! Why shoot now?!";
				}
				else
				{
					if (score >= totalPossibleScore)
						percentagePopup.text = "WOW! You got all " + totalPossibleScore + "0 points! Great work!";
					else
						percentagePopup.text = "That's " + score + "0 out of " + totalPossibleScore + "0 possible points!";
				}
			}
		}

		if (percentageTimer > 0)
		{
			percentageTimer -= Time.smoothDeltaTime;

			if (percentageTimer < 2)
			{
				if (score > 0)
				{
					if (drumRollSource != null)
					{
						if (score == totalPossibleScore)
						{
							drumRollSource.clip = perfectScoreClip;
						}

						drumRollSource.Play();
						drumRollSource = null;
					}

					if (percentageTimer > 0) scoreTally = Mathf.Lerp(0.0f, score, (2.0f - percentageTimer) * 0.5f);
					else scoreTally = score;

					percentage.text = (Mathf.Floor(((float)scoreTally / (float)totalPossibleScore) * 1000.0f) * 0.1f).ToString("F1") + "%";
				}

				float a = fadeMaterial2.GetFloat("_Alpha");
				if (a < 1.0f)
				{
					a += Time.smoothDeltaTime * 2.1f;
					if (a > 1.0f) a = 1.0f;
					fadeMaterial2.SetFloat("_Alpha", a);
				}
			}

			if (percentageTimer < 0)
			{
				fadeMaterial2.SetFloat("_Alpha", 1.0f);
			}
		}




		mouseDown = Input.GetMouseButtonDown(0);



		/*
		if (mouseDown)
		{
			characterIndex++;
			if (characterIndex >= 16) characterIndex = -1;
		}
		*/

		if (previousCharacterIndex != characterIndex)
		{
			previousCharacterIndex = characterIndex;

			fadeOutCharacterMaterial = currentCharacterMaterial;

			if (characterIndex >= 0)
				fadeInCharacterMaterial = characterMaterials[characterIndex];
			else
				fadeInCharacterMaterial = null;

			currentCharacterMaterial = fadeInCharacterMaterial;
		}

		if (fadeInCharacterMaterial != null)
		{
			Color c = fadeInCharacterMaterial.color;

			c.a = Mathf.SmoothDamp(c.a, characterOpacity, ref fadeInSmoothVelocity, characterFadeTime);

			if ((characterOpacity - c.a) < 0.01f)
			{
				c.a = characterOpacity;
				fadeInSmoothVelocity = 0;
				fadeInCharacterMaterial.color = c;
				fadeInCharacterMaterial = null;
			}
			else
			{
				fadeInCharacterMaterial.color = c;
			}
		}

		if (fadeOutCharacterMaterial != null)
		{
			Color c = fadeOutCharacterMaterial.color;

			c.a = Mathf.SmoothDamp(c.a, 0, ref fadeOutSmoothVelocity, characterFadeTime);

			if (c.a < 0.01f)
			{
				c.a = 0;
				fadeOutSmoothVelocity = 0;
				fadeOutCharacterMaterial.color = c;
				fadeOutCharacterMaterial = null;
			}
			else
			{
				fadeOutCharacterMaterial.color = c;
			}
		}


		if (videoPlayerEnd.isPlaying)
		{
			if (unlockClip != null)
			{
				PlaySound(unlockClip, 0.0f);
				unlockClip = null;
			}
		}
	}

	public void LateUpdate()
	{
		mousePositionCorrected = uiCamera.ScreenToWorldPoint(Input.mousePosition - uiCamera.transform.position);

		if (mouseDown && CrosshairDraw.started && !CrosshairDraw.finished)
		{
			Vector3 end = mousePositionCorrected;

			Laser laserInstance = Instantiate<GameObject>(laser).GetComponent<Laser>();
			laserInstance.credit = CreditCollisionBox.hitCredit;
			laserInstance.hitMousePos = mousePositionCorrected;

			laserInstance.start = laserStartOffset;

			if (CreditCollisionBox.hitCredit == null)
			{
				end.x = mousePositionCorrected.x + laserStartOffset.x;

				float increases = 1.0f;

				//laserInstance.shrink = true;
				laserInstance.gameObject.layer = 0;

				Vector3 delta = end - laserInstance.start;
				laserInstance.end = laserInstance.start;
				while (laserInstance.end.z < 3000.0f)
				{
					laserInstance.end += delta;
					increases += 1.0f;
				}

				laserInstance.timeLength *= increases;
				//laserInstance.bufferTime *= (increases + 1.0f);
			}
			else
			{
				end.x = mousePositionCorrected.x + (laserStartOffset.x * endXFactor);

				laserInstance.end = end;
			}

			laserInstance = Instantiate<GameObject>(laser).GetComponent<Laser>();

			laserInstance.start = laserStartOffset2;
			
			if (CreditCollisionBox.hitCredit == null)
			{
				end.x = mousePositionCorrected.x + laserStartOffset2.x;

				float increases = 1.0f;

				//laserInstance.shrink = true;
				laserInstance.gameObject.layer = 0;

				Vector3 delta = end - laserInstance.start;
				laserInstance.end = laserInstance.start;
				while (laserInstance.end.z < 3000.0f)
				{
					laserInstance.end += delta;
					increases += 1.0f;
				}

				laserInstance.timeLength *= increases;
				//laserInstance.bufferTime *= (increases + 1.0f);
			}
			else
			{
				end.x = mousePositionCorrected.x + (laserStartOffset2.x * endXFactor);

				laserInstance.end = end;
			}

			PlaySound(laserSound, 0.4f);

			CreditCollisionBox.hitCredit = null;
		}

        if (videoPlayerEnd.isPlaying)
        {
            float time = (float)videoPlayerEnd.time;
            if (time > 8.75f)
            {
                Color c = timeText.color;
                if (c.a > 0)
                {
                    c.a -= Time.deltaTime * 20.0f;
                    if (c.a < 0) c.a = 0;
                    timeText.color = c;
                }
            }
            else if (time > 0.886f)
            {
                Color c = timeText.color;
                if (c.a < 1)
                {
                    c.a += Time.deltaTime * 6.0f;
                    if (c.a > 1) c.a = 1;
                    timeText.color = c;
                }
            }
        }
	}

	public void PlaySound(AudioClip clip, float randomRange = 0.4f)
	{
        if (clip == null) return;

		AudioSource source = sfxSources[sfxSourceIndex];
		source.clip = clip;
		source.pitch = 1.0f + ((Random.value * randomRange) - (randomRange * 0.5f));
		source.Play();

		sfxSourceIndex++;
		if (sfxSourceIndex >= sfxSourcesCount) sfxSourceIndex = 0;
	}

    public void PlayEndVideo()
    {
        videoPlayerEnd.Play();

        Destroy(GameObject.FindGameObjectWithTag("FadePlane2"));
        Destroy(GameObject.FindGameObjectWithTag("BGVideo"));
        videoPlayer = null;

        timeText.gameObject.SetActive(true);
        Color c = Color.red;
        ColorUtility.TryParseHtmlString("#FFB600", out c);
        c.a = 0;
        timeText.color = c;

#if RECORDING_VERSION
        System.DateTime now = new System.DateTime(2018, 10, 8, 0, 6, 9);
#else
        System.DateTime now = System.DateTime.Now;
#endif

        timeText.text = now.ToString("yyyy.MM.dd    hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);

        videoPlayerEnd.loopPointReached += (VideoPlayer vp) =>
        {
#if RECORDING_VERSION
#elif UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
		    Application.Quit();
#endif
        };
    }
        }
