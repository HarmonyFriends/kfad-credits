﻿//#define REAL_POPUP
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MultiCredit : LargeCredit
{
    List<TextMeshPro> textMeshPros;

	override public void Start()
	{
		base.Start();

        textMeshPros = new List<TextMeshPro>();
        
        int textsCount = transform.childCount;
        for (int i = 0; i < textsCount; i++)
        {
            TextMeshPro tmp = transform.GetChild(i).GetComponent<TextMeshPro>();
            if (tmp != null)
            {
#if REAL_POPUP
                if (tmp.gameObject.name.ToLower() == "popup")
                {
                    if (tmp.gameObject.activeSelf)
                    {
                        popup = tmp.transform;
                        popup.parent = transform.parent;
                        popup.gameObject.SetActive(false);
                        popup.localScale = Vector3.zero;
                    }
                }
                else
                {
                    textMeshPros.Add(tmp);
                }
#else
                textMeshPros.Add(tmp);

                if (tmp.transform.childCount > 0)
                {
                    TextMeshPro[] subTMPs = tmp.GetComponentsInChildren<TextMeshPro>();
                    foreach (TextMeshPro subTMP in subTMPs)
                    {
                        subTMP.transform.SetParent(tmp.transform.parent, true);
                        textMeshPros.Add(subTMP);
                    }
                }
#endif
            }
        }
    }

    public override void SetPopup()
    {
    }

    override public void Go()
    {
        onScreen = true;

        foreach (TextMeshPro tmp in textMeshPros)
        {
            Transform collisionBox = Instantiate<GameObject>(CreditsManager.collisionBox).transform;
            collisionBox.parent = transform;

            collisionBox.localPosition = new Vector3(
                tmp.transform.localPosition.x + tmp.bounds.center.x,
                tmp.transform.localPosition.y - 0.1f,
                tmp.transform.localPosition.z + tmp.bounds.center.y
            );

            collisionBox.localScale = new Vector3(
                (tmp.bounds.extents.x * 2.0f) + 32.0f,
                (tmp.bounds.extents.y * 2.0f) + 32.0f,
                0.1f
            );

            collisionBox.GetComponent<CreditCollisionBox>().credit = this;
        }
    }

    override public void GrayOut()
    {
        if (!grayedOut)
        {
            grayedOut = true;
            poppingUp = (popup != null);

            if (!dontCountInTotal) CreditsManager.score += scoreValue;

            foreach (TextMeshPro textMeshPro in textMeshPros)
            {
                if (textMeshPro.enableVertexGradient)
                    textMeshPro.colorGradientPreset = darkenedGradient;
                else
                    textMeshPro.color = new Color(0.3f, 0.3f, 0.3f);
            }
        }
    }
}
