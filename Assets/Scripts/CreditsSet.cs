﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CreditsSet : MonoBehaviour {

	private Vector3 direction, velocity;
	private float timer = 0.0f;
	private int phase;

	public Vector3 screenSpacesAway;
	public float extraTimeOnScreen = 0.0f;
	public float timeAfterLastAppearance = Mathf.Infinity;

	[HideInInspector]
	public float appearanceTime;

	public bool fastMode;

	public int character = -1;

    [HideInInspector]
    public bool theLastOne = false;

	float centerEpsilonDistance, endStartVelocity, endAccelerationFactor, smoothTime;

	Credit[] credits;

	void Awake()
	{
		phase = 0;
        theLastOne = false;
    }

	void Start()
	{
		if (!fastMode)
		{
			centerEpsilonDistance = 0.049f;
			endStartVelocity = 0.05f;
			endAccelerationFactor = 1.2f;
			smoothTime = 0.45f;
		}
		else
		{
			centerEpsilonDistance = 0.1f;
			endStartVelocity = 0.2f;
			endAccelerationFactor = 1.6f;
			smoothTime = 0.05f;
		}

		float xyfactor = 0;
		if (screenSpacesAway.z > Mathf.Epsilon)
		{
			float foobar = (Mathf.Abs(screenSpacesAway.z) * 8.2f);
			xyfactor = Mathf.Sign(screenSpacesAway.z) * (foobar * foobar * foobar);
		}

		transform.position = new Vector3(
			screenSpacesAway.x * (1920.0f + xyfactor),
			screenSpacesAway.y * (1080.0f + xyfactor),
			screenSpacesAway.z * 499.0f
		);

		direction = (transform.position * -1.0f).normalized;

		credits = GetComponentsInChildren<Credit>();
	}

	public void Go()
	{
		if (phase < 1) phase = 1;

		foreach (Credit credit in credits)
		{
			credit.Go();
			if (!credit.dontCountInTotal) CreditsManager.totalPossibleScore += credit.scoreValue;
		}
	}
	
	void Update()
	{
		if (phase > 0)
		{
			if (phase == 1) // moving to center
			{
				transform.position = Vector3.SmoothDamp(transform.position, Vector3.zero, ref velocity, smoothTime, Mathf.Infinity, Time.smoothDeltaTime);

				float distanceFromCenter = Mathf.Abs(transform.position.magnitude);
				//Debug.Log("Phase 1. Distance from center: " + distanceFromCenter);
				if (distanceFromCenter <= centerEpsilonDistance)
				{
					transform.position = Vector3.zero;
					phase = 2;
				}
			}
			else
			{
				timer += Time.smoothDeltaTime;
				//Debug.Log("Phase " + phase + ". Timer: " + timer);

				if (phase == 2) // staying in center
				{
					if (timer >= extraTimeOnScreen)
					{
						velocity = direction * endStartVelocity;
						timer = 0;
						phase = 3;
						CrosshairDraw.started = true;
					}
				}
				else if (phase == 3) // leaving screen
				{
					transform.position += velocity * Time.smoothDeltaTime;
					if (velocity.magnitude < 20000) velocity *= endAccelerationFactor;

                    if (timer >= 2.5f)
                    {
                        if (theLastOne) CreditsManager.Locate().PlayEndVideo();
                        Destroy(gameObject);
                    }
				}
			}
		}
	}
}
