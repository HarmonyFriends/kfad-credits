﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDraw : MonoBehaviour
{
	public const bool debugEnabled = true;

	public static List<Credit> credits;

	List<Transform> planes;

	public Material whiteMaterial, yellowMaterial, darkGrayMaterial;

	void Awake()
	{
		if (debugEnabled)
		{
			credits = new List<Credit>();
			planes = null;
		}
	}

	void Start()
	{
	}

	void Update()
	{
		if (debugEnabled)
		{
			if (planes == null)
			{
				planes = new List<Transform>();
				foreach (Credit credit in credits)
				{
					Transform plane = GameObject.CreatePrimitive(PrimitiveType.Plane).transform;
					planes.Add(plane);

					plane.GetComponent<Renderer>().material = whiteMaterial;

					plane.parent = transform;
					plane.localPosition = new Vector3(0.0f, 0.0f, -100.0f);
					plane.localScale = new Vector3(10.0f, 1.0f, 10.0f);
					plane.localEulerAngles = new Vector3(-90.0f, 0.0f, 0.0f);
				}
			}
			else
			{
				int count = credits.Count;
				for (int i = 0; i < count; i++)
				{
					Credit credit = credits[i];
					if (credit == null)
					{
						if (planes[i] != null)
						{
							Destroy(planes[i]);
							planes[i] = null;
						}
					}
					else if (credit.onScreen)
					{
						planes[i].localPosition = new Vector3(
							credit.transform.position.x,
							credit.transform.position.y,
							1.0f
						);
					}
				}
			}
		}
	}
}
