﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LargeCredit : Credit
{
	public int startingHP = 5;

	protected bool poppingUp, donePoppingUp;

	[HideInInspector]
	public Transform popup;

	float popupTimer;

	public AudioClip bigBreak, bigChip, swoosh;

	override public void Start()
	{
		base.Start();

		hp = startingHP;
		scoreValue = startingHP;

        poppingUp = false;
        donePoppingUp = false;

        SetPopup();

		popupTimer = 0.0f;

		if (bigBreak == null)
		{
			bigBreak = manager.defaultBigBreakSound;
			bigChip = manager.defaultBigChipSound;
			swoosh = manager.defaultSwooshSound;
		}

		shakeLength = 0.20f;
		shakeIntensity = 32.0f;
	}

    virtual public void SetPopup()
    {
        if (transform.childCount > 0)
        {
            popup = transform.GetChild(0);
            popup.SetParent(transform.parent, true);
            popup.gameObject.SetActive(false);
            popup.localScale = Vector3.zero;
        }
        else
        {
            popup = null;
        }
    }

	override public void Update()
	{
		base.Update();

		if (!donePoppingUp && poppingUp)
		{
			if (popup == null)
			{
				donePoppingUp = true;
			}
			else
			{
				if (popupTimer == 0.0f)
				{
					popup.gameObject.SetActive(true);
					manager.PlaySound(swoosh);
				}

				popupTimer += Time.smoothDeltaTime * 4.0f;
				float scale = Spring(0.0f, 1.0f, popupTimer);
				popup.localScale = Vector3.one * scale;
				if (popupTimer >= 1.0f)
				{
					donePoppingUp = true;
				}
			}
		}

		if ((hp > 0) && (textMeshPro != null))
		{
			Color c = textMeshPro.color;
			if (c.r < 1.0f)
			{
				float v = c.r;
				v += (1.0f - v) * 12.0f * Time.smoothDeltaTime;
				if ((1.0f - v) <= 0.01f) v = 1.0f;

				c.r = v;
				c.g = v;
				c.b = v;

				textMeshPro.color = c;
			}
		}
	}

	override public void Shake()
	{
		if (hp <= 0)
		{
			shakeIntensity *= 1.75f;
			shakeLength *= 1.25f;
		}
		else
		{
			Color c = textMeshPro.color;
			c.r = 0.3f;
			c.g = 0.3f;
			c.b = 0.3f;
			textMeshPro.color = c;
		}
		transform.localPosition = naturalPosition;
		base.Shake();
	}

	override public void Spark(Vector3 hitMousePos)
	{
		if (hp == 0)
		{
			hp = -1;
			MakeSpark(hitMousePos, manager.bigSparks);
		}
		else
		{
			MakeSpark(hitMousePos, manager.sparks);
		}
	}

	float Spring(float from, float to, float time)
	{
		time = Mathf.Clamp01(time);
		time = (Mathf.Sin(time * Mathf.PI * (.2f + 2.5f * time * time * time)) * Mathf.Pow(1f - time, 2.2f) + time) * (1f + (1.2f * (1f - time)));
		return from + (to - from) * time;
	}

	override public void PlayMyHitsound()
	{
		if (hp > 0) manager.PlaySound(bigChip);
		else manager.PlaySound(bigBreak);
	}

    public override void GrayOut()
    {
        base.GrayOut();

        poppingUp = true;
    }
}
