﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Credit : MonoBehaviour
{
	[HideInInspector]
	public int hp = 1;

	[HideInInspector]
	public int scoreValue = 1;

	protected TextMeshPro textMeshPro;

	protected bool grayedOut = false;

	public AudioClip hit;

	protected CreditsManager manager;

	public bool onScreen;

	Bounds bounds;

	public bool dontCountInTotal = false;

	protected TMP_ColorGradient darkenedGradient;

	public float shakeLength = 0.5f;
	public float shakeIntensity = 16.0f;
	float shakeDecayFactor = 0.0f;
	Vector3[] shakeOffsets;
	int shakeOffsetAIndex = 0;
	int shakeOffsetBIndex = 1;
	float shakeTimer = 0.0f;
	float shakeLengthFactor = 2.0f; // (1.0f / shakeLength)
	protected Vector3 naturalPosition;

	const float shakeFrameSpeed = 40.0f;

	virtual public void Start()
	{
		textMeshPro = GetComponent<TextMeshPro>();
		
		manager = CreditsManager.Locate();

		if (hit == null)
		{
			hit = manager.defaultSmallBreakSound;
		}

		onScreen = false;

		darkenedGradient = manager.darkenedGradient;

		shakeOffsets = new Vector3[2];
		naturalPosition = transform.localPosition;
	}

	virtual public void Hit(Vector3 hitMousePos, int damage = 1)
	{
		if (onScreen)
		{
			if (!grayedOut)
			{
				hp -= damage;
				PlayMyHitsound();
				Shake();

				if (hp < 1)
				{
					GrayOut();
				}
			}
			else
			{
				manager.PlaySound(manager.dullHitSound, 0.2f);
			}
		}

		Spark(hitMousePos);
	}

	virtual public void Spark(Vector3 hitMousePos)
	{
		MakeSpark(hitMousePos, manager.sparks);
	}

	virtual public void MakeSpark(Vector3 hitMousePos, GameObject prefab = null)
	{
		if (prefab != null)
		{
			Transform sparks = Instantiate<GameObject>(prefab).transform;
			Vector3 pos = hitMousePos;
			pos.z += 300.0f;
			sparks.position = pos;
		}
	}

	virtual public void GrayOut()
	{
		if (!grayedOut)
		{
			grayedOut = true;

			if (!dontCountInTotal) CreditsManager.score += scoreValue;

			if (textMeshPro.enableVertexGradient)
				textMeshPro.colorGradientPreset = darkenedGradient;
			else
				textMeshPro.color = new Color(0.3f, 0.3f, 0.3f);
		}
	}

	virtual public void PlayMyHitsound()
	{
		manager.PlaySound(hit);
	}

	virtual public void Go()
	{
		onScreen = true;
		
		Transform collisionBox = Instantiate<GameObject>(CreditsManager.collisionBox).transform;
		collisionBox.parent = transform;

		collisionBox.localPosition = textMeshPro.bounds.center;
		collisionBox.localScale = new Vector3(
			(textMeshPro.bounds.extents.x * 2.0f) + 4.0f,
			(textMeshPro.bounds.extents.y * 2.0f) + 4.0f,
			0.1f
		);

		collisionBox.GetComponent<CreditCollisionBox>().credit = this;
	}

	virtual public void Shake()
	{
		shakeTimer = 0.0f;
		shakeDecayFactor = 1.0f;
		shakeLengthFactor = 1.0f / shakeLength;
		shakeOffsetAIndex = 0;
		shakeOffsetBIndex = 1;

		shakeOffsets[0] = Vector3.zero;
		shakeOffsets[1] = Random.insideUnitSphere * shakeIntensity;
	}

	virtual public void Update()
	{
		if (shakeDecayFactor > 0.0f)
		{
			shakeDecayFactor -= Time.smoothDeltaTime * shakeLengthFactor;

			if (shakeDecayFactor <= 0.0f)
			{
				transform.localPosition = naturalPosition;
			}
			else
			{
				shakeTimer += Time.smoothDeltaTime * shakeFrameSpeed;
				if (shakeTimer >= 1.0f)
				{
					while (shakeTimer >= 1.0f)
					{
						shakeTimer -= 1.0f;
					}

					shakeOffsetAIndex = shakeOffsetBIndex;
					shakeOffsetBIndex = (shakeOffsetAIndex == 0 ? 1 : 0);

					shakeOffsets[shakeOffsetBIndex] = Random.insideUnitSphere * shakeIntensity;
				}

				transform.localPosition = naturalPosition + (Vector3.Lerp(shakeOffsets[shakeOffsetAIndex], shakeOffsets[shakeOffsetBIndex], shakeTimer) * shakeDecayFactor);
			}
		}
	}
}
