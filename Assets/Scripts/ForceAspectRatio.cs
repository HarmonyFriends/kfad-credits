﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceAspectRatio : MonoBehaviour
{
	float targetAspect;
	new Camera camera;
	public static float scaleHeight, scaleWidth;
	public static Rect rect;
	float screenWidth, screenHeight;

	void Start()
	{
		targetAspect = 16.0f / 9.0f;

		camera = GetComponent<Camera>();

		screenWidth = -1;
		screenHeight = -1;
	}
	
	void Update()
	{
		if ((screenWidth != (float)Screen.width)
			|| (screenHeight != (float)Screen.height))
		{
			screenWidth = (float)Screen.width;
			screenHeight = (float)Screen.height;

			float currentAspect = screenWidth / screenHeight;

			scaleHeight = currentAspect / targetAspect;
			scaleWidth = 1.0f / scaleHeight;

			rect = camera.rect;

			if (scaleHeight < 1.0f)
			{
				rect.width = 1.0f;
				rect.height = scaleHeight;
				rect.x = 0;
				rect.y = (1.0f - scaleHeight) / 2.0f;
			}
			else
			{
				rect.width = scaleWidth;
				rect.height = 1.0f;
				rect.x = (1.0f - scaleWidth) / 2.0f;
				rect.y = 0;
			}

			camera.rect = rect;
		}
	}
}
